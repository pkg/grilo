Source: grilo
Section: libs
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               meson,
               libglib2.0-dev,
               libgtk-3-dev,
               libxml2-dev,
               liboauth-dev,
               libsoup-3.0-dev,
               libtotem-plparser-dev,
               gobject-introspection,
               libgirepository1.0-dev,
               valac,
               gtk-doc-tools
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://wiki.gnome.org/Projects/Grilo
Vcs-Browser: https://salsa.debian.org/berto/grilo
Vcs-Git: https://salsa.debian.org/berto/grilo.git

Package: libgrilo-0.3-0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Breaks: gnome-games-app (<< 40.0-4.1~),
        gnome-music (<< 44.0-2~),
        grilo-plugins-0.3-base (<< 0.3.16-1.1~),
        rhythmbox (<< 3.4.7),
Suggests: grilo-plugins-0.3
Description: Framework for discovering and browsing media - Shared libraries
 Grilo is a framework focused on making media discovery and browsing
 easy for application developers.
 .
 More precisely, Grilo provides:
   * A single, high-level API that abstracts the differences among
     various media content providers, allowing application developers
     to integrate content from various services and sources easily.
   * A collection of plugins for accessing content from various media
     providers. Developers can share efforts and code by writing
     plugins for the framework that are application agnostic.
   * A flexible API that allows plugin developers to write plugins of
     various kinds.
 .
 This package contains the shared libraries.

Package: libgrilo-0.3-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libgrilo-0.3-0 (= ${binary:Version}),
         gir1.2-grilo-0.3 (= ${binary:Version}),
         libxml2-dev,
         libglib2.0-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: pkg-config
Suggests: libgrilo-0.3-doc
Description: Framework for discovering and browsing media - Development files
 Grilo is a framework focused on making media discovery and browsing
 easy for application developers.
 .
 More precisely, Grilo provides:
   * A single, high-level API that abstracts the differences among
     various media content providers, allowing application developers
     to integrate content from various services and sources easily.
   * A collection of plugins for accessing content from various media
     providers. Developers can share efforts and code by writing
     plugins for the framework that are application agnostic.
   * A flexible API that allows plugin developers to write plugins of
     various kinds.
 .
 This package contains the development files.

Package: libgrilo-0.3-bin
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Framework for discovering and browsing media - Binaries
 Grilo is a framework focused on making media discovery and browsing
 easy for application developers.
 .
 More precisely, Grilo provides:
   * A single, high-level API that abstracts the differences among
     various media content providers, allowing application developers
     to integrate content from various services and sources easily.
   * A collection of plugins for accessing content from various media
     providers. Developers can share efforts and code by writing
     plugins for the framework that are application agnostic.
   * A flexible API that allows plugin developers to write plugins of
     various kinds.
 .
 This package contains the utilities.

Package: libgrilo-0.3-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: devhelp
Conflicts: libgrilo-0.1-doc, libgrilo-0.2-doc
Description: Framework for discovering and browsing media - Documentation
 Grilo is a framework focused on making media discovery and browsing
 easy for application developers.
 .
 More precisely, Grilo provides:
   * A single, high-level API that abstracts the differences among
     various media content providers, allowing application developers
     to integrate content from various services and sources easily.
   * A collection of plugins for accessing content from various media
     providers. Developers can share efforts and code by writing
     plugins for the framework that are application agnostic.
   * A flexible API that allows plugin developers to write plugins of
     various kinds.
 .
 This package contains the documentation.

Package: gir1.2-grilo-0.3
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: Framework for discovering and browsing media - GObject introspection data
 Grilo is a framework focused on making media discovery and browsing
 easy for application developers.
 .
 More precisely, Grilo provides:
   * A single, high-level API that abstracts the differences among
     various media content providers, allowing application developers
     to integrate content from various services and sources easily.
   * A collection of plugins for accessing content from various media
     providers. Developers can share efforts and code by writing
     plugins for the framework that are application agnostic.
   * A flexible API that allows plugin developers to write plugins of
     various kinds.
 .
 This package contains the GObject introspection data. It can be used
 by packages using the GIRepository format to generate dynamic
 bindings.
